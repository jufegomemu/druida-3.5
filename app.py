from flask import Flask, render_template, request
import pandas as pd
from jinja2 import Environment
import os

app = Flask(__name__)
env = Environment()
env.globals.update(len=len)

@app.route('/', methods=['GET', 'POST'])
def index():
    # read Excel file
    excel_file = pd.ExcelFile('Druida APP.xlsx')
    
    sheet_names = excel_file.sheet_names
    
    dataframes = {}
    for sheet_name in sheet_names:
        dataframes[sheet_name] = excel_file.parse(sheet_name)
    
    # read pdf_url file
    with open('pdf_urls.txt', 'r') as f:
        pdf_urls = f.readlines()

    # remove newline characters from pdf urls
    #pdf_urls = [url.strip() for url in pdf_urls]
    pdf_urls  = ["LaQuintaesenciadelDruida.pdf","LibrodelasObrasElevadas.pdf","ManualdeJugador2.pdf","ManualdelJugador1.pdf","ManualdeMiniaturas.pdf","ManualdeMonstruos1.pdf","ManualdeMonstruos2.pdf","ManualdeMonstruos3.pdf"]
    selected_sheet = None
    selected_pdf = None
    selected_pdf_url = None
    
    pdf_titles = ['La Quintaesencia del Druida', "Libro de las Obras Elevadas",'Manual del Jugador 2', 'Manual del Jugador 1', 'Manual de Miniaturas','Manual de Monstruos 1', 'Manual de Monstruos 2', 'Manual de Monstruos 3']

    if request.method == 'POST':
        if 'sheet_name' in request.form:
            selected_sheet = request.form['sheet_name']
        elif 'pdf_index' in request.form:
            selected_pdf = pdf_urls[int(request.form['pdf_index'])]
            selected_pdf_url = selected_pdf

    return render_template('index2.html', sheet_names=sheet_names, dataframes=dataframes, selected_sheet=selected_sheet, pdf_urls=pdf_urls, selected_pdf=selected_pdf,selected_pdf_url= selected_pdf_url, pdf_titles=pdf_titles)

if __name__ == '__main__':
    app.run(debug=True)

"""
import os
import time
 
def print_folder_structure(folder_path, indent=0):
    files  = os.listdir(folder_path)
    for file in files:
        if os.path.isfile(os.path.join(folder_path,file)):
            print(' ' * indent + '|--',file)
        elif os.path.isdir(os.path.join(folder_path,file)):
            print(' ' * indent + '|--',file)
            print_folder_structure(os.path.join(folder_path,file),indent+4)

print_folder_structure(folder_path ="./")
"""